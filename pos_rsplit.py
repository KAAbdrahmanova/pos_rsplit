"""Модуль содержит функции, необходимые для деления строки на список слов  и умеет определять позиции
слов в исходном тексте"""
from typing import List, Dict, Tuple
from tco import *

def find(s:str)->int:
    """Возвращает индекс первого вхождения ' ' в строку, в  которой нет пробелов в начале"""
    s=s.lstrip()
    return s.find(' ')

def dell_empty(l:List[str])->List[str]:
    """Возвращает список, в котором нет символа ''(если он был изначально)"""
    if '' in l: l.remove('')
    return l

def pos_start(main_s:str,s:str)->int:
    """Находит начало слова"""
    word=s.lstrip()[0:find(s)]
    return main_s.find(word)

def pos_end(main_s:str,s:str)->int:
    "Находит конец слова"""
    return pos_start(main_s,s)+len(s.lstrip()[0:find(s)])-1

def del_punctuation(main_s:str,s:str,punct:List[str],del_punct:bool,opt_pos:bool)->List[str]:
    if del_punct==True and s.lstrip()[0:find(s)] in punct: return [] 
    elif opt_pos==True:return [(s.lstrip()[0:find(s)],pos_start(main_s,s),pos_end(main_s,s))]
    else: return[s.lstrip()[0:find(s)]]
    

@with_continuations()
def rsplit_tail_pos(main_s,s:str,y:List,self=None)->List[str]:
    """Реализует деление строки на список слов(символов) и указывает позиции начала и конца слова относительно исходной строки
   Arguments:
       main_s -- исходная строка
       s;y -- текущая строка; массив(изначально пустой) в который будут добавляться слова
   Returns:
       y -- массив со словами и соответствующими позициями начала и конца слова относительно исходной строки"""
    return y+[] if len(s)==0 else y+dell_empty([s.lstrip()]) if find(s)<0 else  self(main_s,(s.lstrip()[find(s)+1:]),y+[(s.lstrip()[0:find(s)],pos_start(main_s,s),pos_end(main_s,s))])

@with_continuations()
def rsplit_tail(s:str,y:List,self=None)->List[str]:
    """Реализует деление строки на список слов(символов)
   Arguments:
       s;y -- текущая строка; массив(изначально пустой) в который будут добавляться слова
   Returns:
       y -- массив со словами"""
    return y+[] if len(s)==0 else y+dell_empty([s.lstrip()]) if find(s)<0 else  self((s.lstrip()[find(s)+1:]),y+[s.lstrip()[0:find(s)]])

@with_continuations()
def rsplit_tail_option_pos(main_str:str,s:str,y:List,opt_pos:bool,self=None)->List[str]:
    """Реализует выбор вариана выполнения функции в зависимости от опции: показывать индексы начала и конца слова или нет"""
    return rsplit_tail_pos(main_str,s,y) if opt_pos==True else rsplit_tail(s,[])

@with_continuations()
def RSPLIT(main_str:str,s:str,y:List,opt_pos:bool,punct:List[str],del_punct:bool,self=None)->List[str]:
    return y+[] if len(s)==0 else y+dell_empty([s.lstrip()]) if find(s)<0 else  self(main_str,(s.lstrip()[find(s)+1:]),y+del_punctuation(main_str,s,punct,del_punct,opt_pos,),opt_pos,punct,del_punct)

if __name__ == '__main__':
    s="   hello, world ! "
    print(rsplit_tail_option_pos(s,s,[],True))
    opt_pos=False
    punct=['!',',']
    del_punct=True
    print(RSPLIT(s,s,[],opt_pos,punct,del_punct))

